package in.co.rshop.ems.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.co.rshop.ems.entities.Department;

@Repository
public interface DepartmentRepo extends JpaRepository<Department, Long> {

}